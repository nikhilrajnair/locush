<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'locush' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ezlRqM#qo4lj*W<Nb5;6UZs^RBMY61wCa*|BQSn5T-ZYg62ZZ7Ck{1tanfq=<,N6' );
define( 'SECURE_AUTH_KEY',  '5{eU&[PH7liDKS.j{&1:2CXq=(AaNr,jWSOIKMB<uKU>>4:yw(6VSF,Rms^qI%q(' );
define( 'LOGGED_IN_KEY',    'oJ0hk^l0d9*!FQ/O?@]3[MadTu:e2:QOr5[lsxc+Q<a^0ny/ID<3g}Ws}5lG/}Z2' );
define( 'NONCE_KEY',        '[0]Dpw_Akxs2>$|iUtK#;[Qd8A%6@3*-Cs1>ht}dU.deG8qs( o}$WyNO< AGa_y' );
define( 'AUTH_SALT',        'l=4TNzQERkT&]rc1D+},oR  >B$E$R])O^TK_;BZr-6Vn?$Zdxe5)2Fg4bdlK(u2' );
define( 'SECURE_AUTH_SALT', 'C+uq}Ab_!.T5&>4m&X`9Wl NW:y5xv[^i+,yKM[Fb_VOM+wD#_+pX/T&y`r>FEq=' );
define( 'LOGGED_IN_SALT',   'TOgyWa`=*0ugazxq&]e2+rV1>YnZhy9D;C^NWYI!,9a#P^YrwOzcA&.Z^T;3P7dp' );
define( 'NONCE_SALT',       '|98[#.!o#Hp4 ?b0~|rCcn~6UUpHqL1G10(KlWXcrc,%!zHLu*k8Y9IB^{I@cyhY' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
