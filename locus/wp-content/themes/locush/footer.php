<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package locush
 */

?>

<footer class="footer mt-5">

<div class="container">
	<div class="row d-none d-md-flex">
		<div class="col-md-9">
			<div class="row">
				<div class="col-md-3">
					<h6 class="ftr-title m-0">PRODUCTS &amp; SERVICES</h6>
					<ul class="list-group">

						<li><a href="#">Locus Dispatcher</a></li>

						<li><a href="#">Locus TrackIQ (Formerly MotionTrack)</a></li>

						<li><a href="#">Locus IntelliSort</a></li>

						<li><a href="#">Locus AccuPin</a></li>

						<li><a href="#">Locus FleetMix</a></li>

						<li><a href="#">Locus Labs</a></li>

					</ul>
				</div>

				<div class="col-md-3">
					<h6 class="ftr-title m-0">BUSINESS SECTORS</h6>
					<ul class="list-group">

						<li><a href="#">E-Commerce</a></li>

						<li><a href="#">3PL</a></li>

						<li><a href="#">Home Services</a></li>

						<li><a href="#">Retail</a></li>

						<li><a href="#">CPG &amp; FMCG</a></li>

					</ul>
				</div>

				<div class="col-md-3">
					<h6 class="ftr-title m-0">FEATURES</h6>
					<ul class="list-group">
						<li><a href="#">Smart Geocoding</a></li>
						<li><a href="#">Dispatch Overview</a></li>
						<li><a href="#">Predictive Alerts</a></li>
						<li><a href="#">Live Tracking</a></li>
						<li><a href="#">Enroute Analytics</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<h6 class="ftr-title m-0">COMPANY</h6>
					<ul class="list-group">
						<li><a href="#">About Us</a></li>
						<li><a href="#">Careers</a></li>
						<li><a href="#">Partners</a></li>
						<li><a href="#">Trust &amp; Security</a></li>
					</ul>
				</div>

			</div>
		</div>
		<div class="col-md-3">
			<h6 class="ftr-title m-0 connect-heading">CONNECT WITH US</h6>
			<ul class="social-icons m-0 mt-4">

				<li><a href="#" target="_blank"><i class="fa fa-facebook-f fa-facebook"></i></a></li>

				<li><a target="_blank"><i class="fa fa-twitter"></i></a></li>

				<li><a target="_blank"><i class="fa fa-youtube"></i></a></li>

				<li><a target="_blank"><i class="fa fa-instagram"></i></a></li>

				<li><a target="_blank"><i class="fa fa-linkedin"></i></a></li>

			</ul>
		</div>
	</div>
	<div class="row second-row">
		<div class="col-md-9">
			<div class="row">



				<div class="col-md-3">
					<h6 class="ftr-title m-0">PRODUCTS &amp; SERVICES</h6>
					<ul class="list-group">
						<li><a href="#">Locus Dispatcher</a></li>
						<li><a href="#">Locus TrackIQ (Formerly MotionTrack)</a></li>
						<li><a href="#">Locus IntelliSort</a></li>
						<li><a href="#">Locus AccuPin</a></li>
						<li><a href="#">Locus FleetMix</a></li>
						<li><a href="#">Locus Labs</a></li>
					</ul>
				</div>

				<div class="col-md-3">
					<h6 class="ftr-title m-0">&nbsp;</h6>
					<ul class="list-group">
						<li><a href="#">E-Commerce</a></li>
						<li><a href="#">3PL</a></li>
						<li><a href="#">Home Services</a></li>
						<li><a href="#">Retail</a></li>
						<li><a href="#">CPG &amp; FMCG</a></li>
					</ul>
				</div>

				<div class="col-md-3">
					<h6 class="ftr-title m-0">FEATURES</h6>
					<ul class="list-group">

						<li><a href="#">Smart Geocoding</a></li>

						<li><a href="#">Dispatch Overview</a></li>

						<li><a href="#">Predictive Alerts</a></li>

						<li><a href="#">Live Tracking</a></li>

						<li><a href="#">Enroute Analytics</a></li>

					</ul>
				</div>
				<div class="col-md-3">
					<h6 class="ftr-title m-0 connect-heading d-none d-lg-block">LANGUAGES</h6>
					<ul class="lang list-group">
						<li>
							<a href="#">
								<img src="<?php echo get_bloginfo('template_url') ?>/img/icons/usa-tr.svg" alt="US_Flag" class="loading"
									>
								<p class="en selected">English</p>
							</a>
						</li>
						<li>
							<a href="#">
								<img src="<?php echo get_bloginfo('template_url') ?>/img/icons/fr.svg" alt="French_Flag" class="loading"
									>
								<p class="fr">French</p>
							</a>
						</li>
						<li>
							<a href="#">
								<img src="<?php echo get_bloginfo('template_url') ?>/img/icons/de.svg" alt="German_Flag" class="loading"
									>
								<p class="de">German</p>
							</a>
						</li>
					</ul>
				</div>

			</div>
		</div>
		<div class="col-md-3">
			<h6 class="ftr-title m-0 tq-sub-dn">STAY TUNED</h6>
			<p class="font-weight-normal tq-sub-dn">Subscribe to our newsletter</p>
			<div class="form-wrapper">
				<form class="single-field-form email-only-signup">
					<div class="email-field">
						<div class="input-group mb-3">
							<input type="text" class="form-control" placeholder="Enter Email ID"
								aria-label="Recipient's username" aria-describedby="basic-addon2">
							<div class="input-group-append">
								<button class="input-group-text cmn-btn" id="basic-addon2">Subscribe</button>
							</div>
						</div>
						<div class="disclamier">
							<input class="form-check-input m-0" type="checkbox" id="subscribe-consentcs"
								value="Yes">
							<small class="privacy-p d-block"> I would also like to subscribe to the Exclusive
								Product Updates, Content &amp; Event invitations and notifications by Locus in
								related areas. You can refer to our&nbsp;
								<a href="#" target="_blank">Privacy
									notice
								</a>
							</small>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<section class="d-flex sub-footer">
	<div class="container">
		<div class="copyright-footer">
			<img src="<?php echo get_bloginfo('template_url') ?>/img/black.svg" style="height:50px" class="footer-logo d-inline"
				alt="Locus">
			<p class="m-0 copyright-text">© 2020 Mara Labs, Inc. All rights reserved. <a target="_blank"
					href="#">Privacy
				</a> and
				<a href="#" target="_blank">
					Terms
				</a>
			</p>
		</div>
	</div>
</section>
</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
