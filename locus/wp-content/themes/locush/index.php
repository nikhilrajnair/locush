<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package locush
 */

get_header();
?>



<main id="primary" class="site-main">


	<section class="home-one">
        <img src="<?php echo get_bloginfo('template_url') ?>/img/banner-bg.png" class="banner-bg" alt="">

        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-10">
                    <div class="wrapper text-center">
                        <h2>Make the right decisions to delight your customers</h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo quia corporis perspiciatis
                            optio repudiandae. Est officiis a molestiae optio tempora consequuntur id, aut quae, natus
                            maiores blanditiis distinctio exercitationem dolor</p>
                        <div class="w-75 mx-auto">
                            <div class="input-group">
                                <span>
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="Search" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="tech-updates">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-capitalize">tech updates</h2>
                </div>
                <div class="col-md-12">
                    <div class="card-block mt-5">
                        <div class="tech-updatescard mt-5">
                            <img src="<?php echo get_bloginfo('template_url') ?>/img/b-1.jpg" alt="Locus">
                            <div class="contents">
                                <h3>Lorem ipsum dolor sit amet</h3>
                                <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                    1500st.Lorem Ipsum is simply dummy text of the printing and typesetting
                                    industry.</span>
                            </div>
                        </div>
                        <div class="tech-updatescard mt-5">
                            <img src="<?php echo get_bloginfo('template_url') ?>/img/b-3.jpg" alt="Locus">
                            <div class="contents">
                                <h3>Lorem ipsum dolor sit amet</h3>
                                <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                    1500st.Lorem Ipsum is simply dummy text of the printing and typesetting
                                    industry.</span>
                            </div>
                        </div>
                        <div class="tech-updatescard mt-5">
                            <img src="<?php echo get_bloginfo('template_url') ?>/img/b-2.jpg" alt="Locus">
                            <div class="contents">
                                <h3>Lorem ipsum dolor sit amet</h3>
                                <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy text ever since the
                                    1500st.Lorem Ipsum is simply dummy text of the printing and typesetting
                                    industry.</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <div class="mt-5">
                        <a href="#" class="btn btn-link">Load More</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="stories">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>people stories</h2>
                </div>
                <div class="col-md-12">
                    <div id="myCarousel" class="carousel slide w-100" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">

                            <div class="carousel-item active pb-5">
                                <div class="row">
                                    <div class="col-sm-4 col-12">
                                        <div class="card">
                                            <img class="card-img-top" src="<?php echo get_bloginfo('template_url') ?>/img/story-1.jpg" alt="Card image cap">
                                            <div class="card-body">
                                                <div class="date">June 24, 2020</div>
                                                <h4 class="card-title">Lorem ipsum dolor sit, amet consectetur
                                                    adipisicing
                                                    elit
                                                </h4>
                                                <a href="#">read more</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-12">
                                        <div class="card">
                                            <img class="card-img-top" src="<?php echo get_bloginfo('template_url') ?>/img/story-2.jpg" alt="Card image cap">
                                            <div class="card-body">
                                                <div class="date">June 24, 2020</div>
                                                <h4 class="card-title">Lorem ipsum dolor sit, amet consectetur
                                                    adipisicing
                                                    elit
                                                </h4>
                                                <a href="#">read more</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-12">
                                        <div class="card">
                                            <img class="card-img-top" src="<?php echo get_bloginfo('template_url') ?>/img/story-3.jpg" alt="Card image cap">
                                            <div class="card-body">
                                                <div class="date">June 24, 2020</div>
                                                <h4 class="card-title">Lorem ipsum dolor sit, amet consectetur
                                                    adipisicing
                                                    elit
                                                </h4>
                                                <a href="#">read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item  pb-5">
                                <div class="row">
                                    <div class="col-sm-4 col-12">
                                        <div class="card">
                                            <img class="card-img-top" src="<?php echo get_bloginfo('template_url') ?>/img/story-1.jpg" alt="Card image cap">
                                            <div class="card-body">
                                                <div class="date">June 24, 2020</div>
                                                <h4 class="card-title">Lorem ipsum dolor sit, amet consectetur
                                                    adipisicing
                                                    elit
                                                </h4>
                                                <a href="#">read more</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-12">
                                        <div class="card">
                                            <img class="card-img-top" src="<?php echo get_bloginfo('template_url') ?>/img/story-2.jpg" alt="Card image cap">
                                            <div class="card-body">
                                                <div class="date">June 24, 2020</div>
                                                <h4 class="card-title">Lorem ipsum dolor sit, amet consectetur
                                                    adipisicing
                                                    elit
                                                </h4>
                                                <a href="#">read more</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-12">
                                        <div class="card">
                                            <img class="card-img-top" src="<?php echo get_bloginfo('template_url') ?>/img/story-3.jpg" alt="Card image cap">
                                            <div class="card-body">
                                                <div class="date">June 24, 2020</div>
                                                <h4 class="card-title">Lorem ipsum dolor sit, amet consectetur
                                                    adipisicing
                                                    elit
                                                </h4>
                                                <a href="#">read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="stories">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>newest from gallery</h2>
                </div>
            </div>
            <div class="row mx-auto my-auto">
                <div id="myCarousel1" class="carousel slide w-100" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">

                        <div class="carousel-item active pb-5">
                            <div class="row">
                                <div class="col-sm-4 col-12">
                                    <div class="card">
                                        <img class="card-img-top" src="<?php echo get_bloginfo('template_url') ?>/img/story-1.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <div class="date">June 24, 2020</div>
                                            <h4 class="card-title">Lorem ipsum dolor sit, amet consectetur adipisicing
                                                elit
                                            </h4>
                                            <a href="#">read more</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-12">
                                    <div class="card">
                                        <img class="card-img-top" src="<?php echo get_bloginfo('template_url') ?>/img/story-2.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <div class="date">June 24, 2020</div>
                                            <h4 class="card-title">Lorem ipsum dolor sit, amet consectetur adipisicing
                                                elit
                                            </h4>
                                            <a href="#">read more</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-12">
                                    <div class="card">
                                        <img class="card-img-top" src="<?php echo get_bloginfo('template_url') ?>/img/story-3.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <div class="date">June 24, 2020</div>
                                            <h4 class="card-title">Lorem ipsum dolor sit, amet consectetur adipisicing
                                                elit
                                            </h4>
                                            <a href="#">read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item  pb-5">
                            <div class="row">
                                <div class="col-sm-4 col-12">
                                    <div class="card">
                                        <img class="card-img-top" src="<?php echo get_bloginfo('template_url') ?>/img/story-1.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <div class="date">June 24, 2020</div>
                                            <h4 class="card-title">Lorem ipsum dolor sit, amet consectetur adipisicing
                                                elit
                                            </h4>
                                            <a href="#">read more</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-12">
                                    <div class="card">
                                        <img class="card-img-top" src="<?php echo get_bloginfo('template_url') ?>/img/story-2.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <div class="date">June 24, 2020</div>
                                            <h4 class="card-title">Lorem ipsum dolor sit, amet consectetur adipisicing
                                                elit
                                            </h4>
                                            <a href="#">read more</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-12">
                                    <div class="card">
                                        <img class="card-img-top" src="<?php echo get_bloginfo('template_url') ?>/img/story-3.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <div class="date">June 24, 2020</div>
                                            <h4 class="card-title">Lorem ipsum dolor sit, amet consectetur adipisicing
                                                elit
                                            </h4>
                                            <a href="#">read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#myCarousel1" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#myCarousel1" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

        </div>
    </section>


    <section class="newsletter">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-10">
                    <div class="wrapper text-center">
                        <h2>Subscribe to our Newsletter</h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo quia corporis perspiciatis
                            optio repudiandae. Est officiis a molestiae optio tempora consequuntur id, aut quae, natus
                            maiores blanditiis distinctio exercitationem dolor</p>
                        <div class="w-75 mx-auto form-holder">
                            <div class="row">
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="Enter your email" />
                                </div>
                                <div class="col-md-4">
                                    <button class="btn btn-primary btn-block">
                                        Subscribe
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                if ( have_posts() ) :

                    if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
				<?php
                    endif;

                    /* Start the Loop */
                    while ( have_posts() ) :
                        the_post();

                        /*
                        * Include the Post-Type-specific template for the content.
                        * If you want to override this in a child theme, then include a file
                        * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                        */
                        get_template_part( 'template-parts/content', get_post_type() );

                    endwhile;

                    the_posts_navigation();

                else :

                    get_template_part( 'template-parts/content', 'none' );

                endif;
                ?>

            </div>
        </div>
    </div>
</main>

<?php

// get_sidebar();
get_footer();
