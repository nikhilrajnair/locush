
<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package locush
 */

?>

<div class="card mt-5">
    <img src="<?php echo get_bloginfo('template_url') ?>/img/b-2.jpg" alt="Locus">
    <div class="contents">
        <h3>Lorem ipsum dolor sit amet</h3>
        <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industry's standard dummy text ever since the
            1500st.Lorem Ipsum is simply dummy text of the printing and typesetting
            industry.</span>
    </div>
</div>
