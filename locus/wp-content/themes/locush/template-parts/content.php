<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package locush
 */

?>




	

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="tech-updatescard mt-5">
	<img src="<?php echo get_bloginfo('template_url') ?>/img/b-3.jpg" alt="Locus">
	<div class="contents">
	<?php
	if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
            
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		?>
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'locush' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			)
		);

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'locush' ),
				'after'  => '</div>',
			)
		);
		?>

	</div>
</div>
	
</article><!-- #post-<?php the_ID(); ?> -->
