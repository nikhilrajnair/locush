<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package locush
 */

get_header();
?>


<main id="primary" class="site-main detailed-blog">

    <div class="container-fluid px-5 mt-5">
        <div class="row">
            <div class="col-md-8">
            <?php
                while ( have_posts() ) :
                    the_post();

                    get_template_part( 'template-parts/content' );

                endwhile; // End of the loop.
                ?>
            </div>

            <div class="col-md-4">
                <?php	get_sidebar(); ?>
            </div>
        </div>
    </div>
    

</main><!-- #main -->

<section class="newsletter">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-10">
                <div class="wrapper text-center">
                    <h2>Subscribe to our Newsletter</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo quia corporis perspiciatis
                        optio repudiandae. Est officiis a molestiae optio tempora consequuntur id, aut quae, natus
                        maiores blanditiis distinctio exercitationem dolor</p>
                    <div class="w-75 mx-auto form-holder">
                        <div class="row">
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="Enter your email" />
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-primary btn-block">
                                    Subscribe
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
