<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package locush
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">
	<div class="subscribe-form">
		<form action="">
			<h4>Subscribe to Locus</h4>
			<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat, perspiciatis</p>
			<div class="form-group">
				<input type="text" class="form-control" id="name" placeholder="Name">
			</div>
			<div class="form-group">
				<input type="email" class="form-control" id="name" placeholder="Bussiness Email">
			</div>
			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<button class="btn btn-outline-secondary dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img
							src="<?php echo get_bloginfo('template_url') ?>/img/icons/usa-tr.svg" height="20px" alt=""></button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="#">
							<img src="<?php echo get_bloginfo('template_url') ?>/img/icons/de.svg" height="20px" alt="">
						</a>
						<a class="dropdown-item" href="#">
							<img src="<?php echo get_bloginfo('template_url') ?>/img/icons/fr.svg" height="20px" alt="">
						</a>
					</div>
				</div>
				<input type="text" class="form-control" placeholder="Contact Nummber">
			</div>
			<div class="disclamier">
				<input class="form-check-input m-0" type="checkbox" id="subscribe-consentcs"
					value="Yes">
				<small class="privacy-p d-block"> I would also like to subscribe to the Exclusive
					Product Updates, Content &amp; Event invitations and notifications by Locus in
					related areas. You can refer to our&nbsp;
					<a href="/privacy-policy/" target="_blank">Privacy
						notice
					</a>
				</small>
			</div>
			<div class="form-group mt-4">
				<button class="btn btn-primary">Submit</button>
			</div>
		</form>
	</div>
</aside><!-- #secondary -->
